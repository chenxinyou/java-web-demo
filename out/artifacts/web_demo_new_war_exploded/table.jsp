<%@ page import="com.cxy.pojo.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <title>Title</title>
    <script src="js/jquery-1.11.1.js"></script>
    <link rel="stylesheet" href="font/iconfont.css">
    <script src="js/table.js"></script>
    <link rel="stylesheet" href="css/table.css"/>
</head>
<body>
    <%
        HttpSession mySession = request.getSession();
        User user = (User) mySession.getAttribute("user");
    %>
<div class="table">
    <h1>欢迎:<%=user!=null?user.getNickName():""%></h1>
    <table border="1px solid" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>
                <input type="checkbox" id="all"/>
                <span>id</span>
            </td>
            <td>user_name</td>
            <td>pass_word</td>
            <td>nick_name</td>
            <td>create_time</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <%
            List<User> users = (List<User>) request.getAttribute("users");
            for (User u : users) {
        %>
        <tr>
            <td>
                <input type="checkbox"/>
                <span><%= u.getId()%></span>
            </td>
            <td><%= u.getUserName()%>
            </td>
            <td><%= u.getPassWord()%>
            </td>
            <td><%= u.getNickName()%>
            </td>
            <td><%= u.getCreateTime()%>
            </td>
            <td><a href="javascript:toEdit(<%=u.getId()%>)">修改</a>|
                <a href="javascript:deleteById(<%=u.getId()%>)">删除</a>
            </td>
        </tr>
        <%}%>
        </tbody>
    </table>
    <a href="index.jsp">返回首页</a>
    <a href="javascript:deleteAll()">批量删除</a>
    <a href="javascript:addUser()">注册用户</a>
    <p>根据用户名查询:<input type="text" name="search" id="search"/></p>
    <h1><a href="user/logOut">退出登录</a></h1>
</div>
<div class="zhezhao"></div>
<div class="edit">
    <div class="formDiv">
        <form action="user/edit" method="post">
            <input type="hidden" value='<%=request.getAttribute("user")%>' id="user"/>
            <input type="hidden" name="id" value="" id="userId"/>
            用户名:<input type="text" name="userName" id="userName"/> <br>
            密码:<input type="password" name="passWord" id="passWord"/> <br>
            姓名:<input type="text" name="nickName" id="nickName"/> <br>
            <input type="submit" value="修改">
        </form>
    </div>
    <span class="close iconfont" id="closeSpan">&#xe639;</span>
</div>

<div class="add">
    <div class="formDiv">
        <form action="user/add" method="post">
            用户名:<input type="text" name="userName" id="userName1"/><span class="span" id="addSpanUser"></span>
            <br>
            密码:<input type="password" name="passWord" id="passWord1"/>
            <br>
            姓名:<input type="text" name="nickName" id="nickName1"/>
            <br>
            验证码:<input type="text" name="inputCode" id="inputCode"/><img src="<%=basePath%>ajax/getCode" id="code">
            <br>
            <span class="span" id="addCode"></span>
            <br>
            <input type="button" value="注册" id="submit"/>
        </form>
    </div>
    <span class="close iconfont" id="closeSpan1">&#xe639;</span>
</div>
</body>
</html>