<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
    <base href="<%=basePath%>">
    <title>登录</title>
    <script src="js/jquery-1.11.1.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" href="css/login.css"/>
</head>
<body>

    <%
        String userName = null;
        String passWord = null;
        String remember = null;
        String msg = (String)request.getAttribute("msg");
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if ("userName".equals(cookie.getName())){
                    userName = cookie.getValue();
                }else if ("passWord".equals(cookie.getName())){
                    passWord = cookie.getValue();
                }else if ("remember".equals(cookie.getName())){
                    remember = cookie.getValue();
                }
            }
        }
    %>


    <form method="POST" action="user/login">
        <p>用户名:<input type="text" name="userName" value="<%=userName!=null ? userName:""%>"></p>
        <p>密 码:<input type="password" name="passWord" value="<%=passWord!=null ? passWord:""%>"></p>
        <p>验证码:<input type="text" name="verifyCode"/>  <img src="<%=basePath%>ajax/getCode" id="code"> <span class="span"></span></p>
        <input type="checkbox" name="remember"  <%=remember!=null ? "checked" :"" %>/> 记住我
        <p><input type="submit"></p>
    </form>
    <p><%=msg!=null?msg:""%></p>
</body>
    <script type="text/javascript">

        let span = $("span");
        let inputCode  = $("input[name='verifyCode']");
        let submit = $(":submit")
        let userName  = $("input[name='userName']");
        let passWord  = $("input[name='passWord']");
        let remember = $(":checkbox");
        const basePath = $("base")[0].baseURI;

        submit.attr("disabled","disabled");
        //点击切换验证码
        var $code = $("#code").click(function () {
            $code.attr("src",basePath+"ajax/getCode")
        });

        //校验验证码
        inputCode.blur(function () {
            if (inputCode.val() !== ""){
                $.get("ajax/verifyCode",{code:inputCode.val()},function(resp) {
                    if (resp.code === 200){
                        span.html(resp.msg);
                        span.attr("class","ok")
                        code = true;
                        submit.removeAttr("disabled");
                    }else{
                        span.html(resp.msg);
                        span.attr("class","error");
                        $code.click();
                        submit.attr("disabled","disabled");
                    }
                });
            }else{
                span.text("请输入验证码");
                span.attr("class", "error");
                submit.attr("disabled","disabled");
            }
        });
        inputCode.focus(function () {
            span.attr("class", "span");
        });

    </script>
</html>