$(function () {
    const $tbody = $("tbody>tr");
    const basePath = document.querySelector("base").baseURI;
    const user = document.querySelector("#user").value;
    const userObj = JSON.parse(user);

    gray();

    //关闭弹窗
    $("#closeSpan").click(function () {
        $(".zhezhao").removeClass("open");
        $(".edit").removeClass("open");
    });
    $("#closeSpan1").click(function () {
        $(".zhezhao").removeClass("open");
        $(".add").removeClass("open");
    });

    //复选框操作
    checked();

    //模糊查询
    let search = $("input[name='search']");
    //实时监控input
    search.on('input propertychange', function () {
        $.get("ajax/search", {keyword: search.val()}, function (resp) {
            let html = "";
            if (resp.code === 200) {
                let users = resp.data;
                console.log(users)
                for (let i = 0; i < users.length; i++) {
                    html += "<tr>";
                    html += "<td><input type='checkbox'/><span>" + users[i].id + "</span></td>"
                    html += "<td>" + users[i].userName + "</td>"
                    html += "<td>" + users[i].passWord + "</td>"
                    html += "<td>" + users[i].nickName + "</td>"
                    html += "<td>" + users[i].createTime + "</td>"
                    html += "<td><a href='javascript:toEdit("+ users[i].id + ")'"+">修改</a>|<a href='javascript:deleteById(" + users[i].id + ")'>删除</a></td>";
                    html += "</tr>";
                }
                $("tbody").html(html);
                gray();
                checked();
            }
        });

    });
})

function gray() {
    let $tr = $("tbody>tr:even");
    for (let i = 0; i < $tr.length; i++) {
        $($tr[i]).css({color: "black", background: "gray"});
    }
}

function toEdit(id) {
    $(".zhezhao").addClass("open");
    $(".edit").addClass("open");
    $.get("user/toEdit", {id: id}, function (resp) {
        resp = JSON.parse(resp);
        console.log(resp)
        if (resp.code === 200) {
            $("#userId").val(resp.data.id);
            $("#userName").val(resp.data.userName);
            $("#passWord").val(resp.data.passWord);
            $("#nickName").val(resp.data.nickName);
        } else {
            alert(resp.msg)
        }
    })
};

function deleteById(id) {
    if (confirm("你确定要删除该条数据吗?")) {
        $.get("user/delete", {id: id}, function (resp) {
            resp = JSON.parse(resp);
            if (resp.code === 200) {
                alert(resp.msg);
                window.location = 'user/test';
            } else {
                alert(resp.msg);
            }
        });
    }
}

//批量删除
function deleteAll() {

    let isDelete = false;
    let checkboxs = $("tbody>tr>td>input");

    for (let i = 0; i < checkboxs.length; i++) {
        if (checkboxs[i].checked) {
            isDelete = true;
        }
    }
    if (isDelete) {
        if (confirm("你确定要删除所选项吗?")) {
            let spans = $("tbody>tr>td>input").siblings();
            let ids = ""; //0,1,2,3....
            for (let i = 0; i < checkboxs.length; i++) {
                if (checkboxs[i].checked) {
                    ids += $(checkboxs[i]).siblings().text() + ","
                }
            }
            $.post("user/deleteAll", {ids: ids}, function (resp) {
                resp = JSON.parse(resp);
                if (resp.code === 200) {
                    alert("批量删除成功")
                    window.location = "user/test";
                } else {
                    alert("批量删除错误")
                }
            })
        } else {
            alert("操作取消")
        }
    } else {
        alert("当前没有勾选")
    }

}

//添加user
function addUser() {
    $(".zhezhao").addClass("open");
    $(".add").addClass("open");

    //开关标记
    var user = false;
    var code = false;

    var span = $("#addSpanUser");
    var span2 = $("#addCode");

    var inputCode = $("#inputCode");
    var submit = $("#submit");

    let basePath = document.querySelector("base").baseURI
    console.log(basePath)

    //点击切换验证码
    var $code = $("#code").click(function () {
        $code.attr("src", basePath+"ajax/getCode")
    });

    $code.click();

    //校验验证码
    inputCode.blur(function () {
        if (inputCode.val() !== "") {
            $.get("ajax/verifyCode", {code: inputCode.val()}, function (resp) {
                if (resp.code === 200) {
                    span2.html(resp.msg);
                    span2.attr("class", "ok")
                    code = true;
                } else {
                    span2.html(resp.msg);
                    span2.attr("class", "error");
                    $code.click();
                    code = false;
                }
            });
        } else {
            span2.text("请输入验证码");
            span2.attr("class", "error");
            code = false;
        }
    });
    inputCode.focus(function () {
        span2.attr("class", "span");
    });

    //校验用户名
    let userName = $("#userName1");
    userName.blur(function () {
        if (userName.val() !== "") {
            $.get("ajax/yz", {userName: userName.val()}, function (resp) {
                if (resp.code === 200) {
                    span.html(resp.msg);
                    span.attr("class", "ok")
                    user = true;
                } else {
                    span.html(resp.msg);
                    span.attr("class", "error");
                    user = false;
                }
            });
        } else {
            span.text("此项为必填项");
            span.attr("class", "error");
            user = false;
        }
    });
    userName.focus(function () {
        span.attr("class", "span");
    });

    //验证输入并提交注册
    submit.click(function () {
        if (user && code) {
            $.post("user/add", {
                userName: userName.val(),
                passWord: $("#passWord1").val(),
                nickName: $("#nickName1").val()
            }, function (resp) {
                resp = JSON.parse(resp);
                if (resp.code === 200) {
                    alert(resp.msg);
                    window.location = 'user/test';
                } else {
                    alert(resp.msg);
                }
            });
        } else {
            alert("填写有误")
            $code.click();
        }
    });

}

function checked() {
    //获取tbody下的CheckBox
    let boxs = $("tbody>tr input");
    //复选框操作
    let flag = true;
    let num = 0;
    $(":checkbox:first").click(function () {
        if (flag) {
            for (let i = 0; i < boxs.length; i++) {
                boxs[i].checked = true;
            }
            num = boxs.length;
            flag = false;
        } else {
            for (let i = 0; i < boxs.length; i++) {
                boxs[i].checked = false;
            }
            flag = true;
            num = 0;
        }
    });
    for (let i = 0; i < boxs.length; i++) {
        $(boxs[i]).click(function () {
            if (boxs[i].checked) {
                num++;
            } else {
                num--;
            }
            //如果全部打钩就勾选id
            if (num === boxs.length) {
                $(":checkbox:first")[0].checked = true;
            } else {
                $(":checkbox:first")[0].checked = false;
            }
        });
    }
}