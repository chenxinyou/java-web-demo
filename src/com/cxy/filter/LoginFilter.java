package com.cxy.filter;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * @author 26414
 */
@WebFilter("/*")
public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig config) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        response.setContentType("text/html;charset=utf-8");
        //获取请求路径
        String requestURI = request.getRequestURI();
        //获取项目名称
        String contextPath = request.getContextPath();

        //截取接口  /web-new/ajax/getCode   得到/ajax/getCode
        String url = requestURI.substring( contextPath.length());

        System.out.println(url);

        //进行判断  /user/login则放行
        if (Objects.equals("/",url)){
            chain.doFilter(request, response);
        }else if(Objects.equals("/login.jsp",url)){
            chain.doFilter(request, response);
        }else if(Objects.equals("/index.jsp",url)){
            chain.doFilter(request, response);
        }else if(Objects.equals("/ajax/getCode",url)){
            chain.doFilter(request, response);
        }else if(Objects.equals("/ajax/verifyCode",url)){
            chain.doFilter(request, response);
        }else if(Objects.equals("/login",url)){
            chain.doFilter(request, response);
        }else if(url.contains("css")){
            chain.doFilter(request, response);
        }else if(url.contains("js")){
            chain.doFilter(request, response);
        }else{
            //如果不是就验证是否登录

            HttpSession session = request.getSession();
            if (session.getAttribute("user") != null){
                chain.doFilter(req, response);
            }else{
                PrintWriter out = response.getWriter();
                out.write("<script>");
                out.write("alert('当前未登录');");
                out.write("location.href = '"+request.getContextPath()+"/login.jsp';");
                out.write("</script>");
                out.close();
            }
        }


    }
}
