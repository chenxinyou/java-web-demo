package com.cxy.dao;

import com.cxy.pojo.User;
import com.cxy.utils.DruidUtil;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.RowProcessor;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * @author 26414
 */
public class UserDao {

    public List<User> findAll() {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        RowProcessor processor = new BasicRowProcessor(new GenerousBeanProcessor());
        String sql = "SELECT * FROM test.tb_user";

        try {
            List<User> users = queryRunner.query(sql, new BeanListHandler<>(User.class,processor));
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int addUser(User user) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        String sql = "INSERT INTO test.tb_user(user_name, pass_word, nick_name, create_time) values(?,?,?,?)";
        try {
            int row = queryRunner.update(sql, user.getUserName(), user.getPassWord(), user.getNickName(), user.getCreateTime());
            return row;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public User findUserById(String id) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        String sql = "SELECT * FROM test.tb_user where id = ?";
        RowProcessor processor = new BasicRowProcessor(new GenerousBeanProcessor());

        try {
            return queryRunner.query(sql, new BeanHandler<>(User.class, processor), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public int editUser(User user) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());

        String sql = "UPDATE test.tb_user set user_name = ?, pass_word = ?,nick_name = ?,create_time = ? where id = ?";

        int row = 0;
        try {
            row = queryRunner.update(sql,user.getUserName(),user.getPassWord(),user.getNickName(),user.getCreateTime(),user.getId());
            return row;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return row;
    }


    public int delete(String id) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        String sql = "delete from test.tb_user where id = ?";
        try {
            int row = queryRunner.update(sql, id);
            return row;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public User ajaxYZ(String userName) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        String sql = "select user_name from test.tb_user where user_name = ?";

        BasicRowProcessor processor = new BasicRowProcessor(new GenerousBeanProcessor());

        try {
            User user = queryRunner.query(sql, new BeanHandler<>(User.class, processor), userName);
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void deleteAll(String s) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        String sql = "delete from test.tb_user where id = ?";
        try {
            queryRunner.update(sql,s);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<User> searchByKeyword(String keyword) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());

        BasicRowProcessor processor = new BasicRowProcessor(new GenerousBeanProcessor());

        String sql = "select * from test.tb_user where  user_name like ?";
        keyword = "%"+keyword+"%";
        try {
            List<User> users = queryRunner.query(sql, new BeanListHandler<>(User.class, processor), keyword);
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User login(String userName, String passWord) {
        QueryRunner queryRunner = new QueryRunner(DruidUtil.getDataSource());
        BasicRowProcessor processor = new BasicRowProcessor(new GenerousBeanProcessor());
        String sql = "select * from test.tb_user where user_name = ? and  pass_word  = ?";

        try {
            User user = queryRunner.query(sql, new BeanHandler<>(User.class, processor), userName, passWord);
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
