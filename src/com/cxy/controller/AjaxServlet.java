package com.cxy.controller;

import com.alibaba.fastjson.JSON;
import com.cxy.pojo.User;
import com.cxy.service.UserService;
import com.cxy.service.impl.UserServiceImpl;
import com.cxy.utils.KaptchaUtil;
import com.cxy.utils.Result;
import com.cxy.utils.StatusCode;
import com.google.code.kaptcha.Producer;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;


/**
 * 相应ajax请求
 * @author 26414
 */
public class AjaxServlet extends HttpServlet {

    private UserService userService;

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json;charset=utf-8");

        String path = request.getServletPath();
        if (Objects.equals("/ajax/yz",path)){
            PrintWriter out = response.getWriter();
            yz(request, out);
        }else if (Objects.equals("/ajax/getCode",path)){
            //生成验证码返回网页
            getCode(request, response);
        }else if (Objects.equals("/ajax/verifyCode",path)){
            verifyCode(request, response);
        }else if (Objects.equals("/ajax/search",path)){
            findByKeyword(request, response);
        }else if (Objects.equals("/ajax/login",path)){

        }
    }

    private void findByKeyword(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String keyword = request.getParameter("keyword");
        PrintWriter out = response.getWriter();
        userService = new UserServiceImpl();
        List<User> users = null;

        if (!"".equals(keyword)){
            users = userService.searchByKeyword(keyword);
        }else{
            users = userService.findAll();
        }
        if (users != null && users.size()>0){
            out.write(JSON.toJSONString(new Result(StatusCode.OK,"关键字查询成功",users)));
        }else {
            out.write(JSON.toJSONString(new Result(StatusCode.ERROR,"关键字查询失败",null)));
        }
    }

    private void verifyCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        String inputCode = request.getParameter("code");

        HttpSession session = request.getSession();
        String code = (String) session.getAttribute("code");
        if (inputCode != null && Objects.equals(inputCode,code)){
            out.write(JSON.toJSONString(new Result(200,"验证码正确",null)));
        }else{
            out.write(JSON.toJSONString(new Result(500,"验证码错误",null)));
        }
        out.close();
    }

    private void getCode(HttpServletRequest request, HttpServletResponse response) {
        Producer captchaProducerMath = KaptchaUtil.getKaptchaBeanMath();
        ServletOutputStream out = null;
        try {
            HttpSession session = request.getSession();
            response.setDateHeader("Expires", 0);
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            response.setContentType("image/jpeg");

            String capStr = null;
            String code = null;
            BufferedImage bi = null;
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            bi = captchaProducerMath.createImage(capStr);

            session.setAttribute("code", code);

            out = response.getOutputStream();
            ImageIO.write(bi, "jpg", out);
            out.flush();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void yz(HttpServletRequest request, PrintWriter out) {
        String userName = request.getParameter("userName");
        UserService userService = new UserServiceImpl();
        User user = userService.ajaxYZ(userName);

        if (user != null) {
            out.write(JSON.toJSONString(new Result(StatusCode.ERROR,"用户名不可用",null)));
        }else {
            out.write(JSON.toJSONString(new Result(StatusCode.OK,"用户名可用",null)));
        }
        out.close();
    }

}
