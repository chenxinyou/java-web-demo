package com.cxy.controller;

import com.alibaba.fastjson.JSON;
import com.cxy.pojo.User;
import com.cxy.service.UserService;
import com.cxy.service.impl.UserServiceImpl;
import com.cxy.utils.Result;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author 26414
 */
public class UserServlet extends HttpServlet {
    private UserService userService;


    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String servletPath = request.getServletPath();
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();


        if (Objects.equals("/user/test",servletPath)){
            userService = new UserServiceImpl();
            query(out,request,response);
            out.close();
        }else if (Objects.equals("/user/add",servletPath)){
            add(request, out);
            out.close();
        }else if (Objects.equals("/user/toEdit",servletPath)){
            toEdit(request, response, out);
            out.close();
        }else if (Objects.equals("/user/edit",servletPath)){
            edit(request, out,response);
            out.close();
        }else if (Objects.equals("/user/delete",servletPath)){
            delete(request, out);
            out.close();
        }else if (Objects.equals("/user/deleteAll",servletPath)){
            deleteAll(request, out);
        }else if (Objects.equals("/login",servletPath)) {
            login(request, response);
        }else if (Objects.equals("/user/logOut",servletPath)){
            logOut(request, response);
        }
    }

    private void logOut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.getSession().removeAttribute("user");
        response.sendRedirect(request.getContextPath()+"/login.jsp");
    }

    private void login(HttpServletRequest request, HttpServletResponse response) {

        String userName = request.getParameter("userName");
        String passWord = request.getParameter("passWord");
        String remember = request.getParameter("remember");
        if (!"".equals(userName) && !"".equals(passWord)) {
            userService = new UserServiceImpl();
            User user = userService.login(userName, passWord);
            if (user != null) {

                Cookie username = null;
                Cookie password = null;
                Cookie rm = null;
                if (remember == null) {
                    username = new Cookie("userName","");
                    password = new Cookie("passWord", "");
                    rm = new Cookie("remember","");
                    username.setMaxAge(0);
                    password.setMaxAge(0);
                    rm.setMaxAge(0);
                }else{
                    username = new Cookie("userName", user.getUserName());
                    password = new Cookie("passWord", user.getPassWord());
                    rm = new Cookie("remember", remember);
                    username.setMaxAge(60*60);
                    password.setMaxAge(60*60);
                    rm.setMaxAge(60*60);

                    HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(60*60);
                    session.setAttribute("user",user);

                }
                username.setPath("/");
                password.setPath("/");
                rm.setPath("/");

                response.addCookie(username);
                response.addCookie(password);
                response.addCookie(rm);

                try {
                    response.sendRedirect("user/test");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                request.setAttribute("msg", "账号或密码错误");
                try {
                    request.getRequestDispatcher("/login.jsp").forward(request, response);
                } catch (ServletException | IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private void deleteAll(HttpServletRequest request, PrintWriter out) {
        String ids = request.getParameter("ids");
        String[] id = ids.split(",");

        userService = new UserServiceImpl();
        boolean bool =  userService.deleteAll(id);
        if (bool){
            out.write(JSON.toJSONString(new Result(200,"批量删除成功",null)));
        }else{
            out.write(JSON.toJSONString(new Result(500,"批量删除失败",null)));
        }
    }

    private void delete(HttpServletRequest request, PrintWriter out) {
        String id = request.getParameter("id");

        userService = new UserServiceImpl();

        boolean bool = userService.delete(id);
        if (bool){
            out.write(JSON.toJSONString(new Result(200,"删除成功",null)));
        }else{
            out.write(JSON.toJSONString(new Result(500,"删除失败",null)));
        }
    }

    private void edit(HttpServletRequest request, PrintWriter out,HttpServletResponse response) {
        response.setContentType("text/html;charsrt=utf-8");
        String id = request.getParameter("id");
        String userName = request.getParameter("userName");
        String passWord = request.getParameter("passWord");
        String nickName = request.getParameter("nickName");

        userService = new UserServiceImpl();

        boolean bool = userService.editUser(new User(Integer.parseInt(id),userName,passWord, nickName,new Date()));
        if (bool){
            out.write("<script>");
            out.write(" window.location = 'test';");
            out.write("alert('修改成功');");
            out.write("</script>");
        }else{
            out.write("修改失败");
        }

    }

    private void toEdit(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws IOException {
        String id = request.getParameter("id");

        if (Objects.equals("",id)){
            out.write("id为null");
            out.close();
            return;
        }
        //根据id查找信息
        userService = new UserServiceImpl();

        User user = userService.findUserById(id);

        if (user != null){
            String strUser = JSON.toJSONString(user);
            request.setAttribute("user",strUser);
            out.write(JSON.toJSONString(new Result(200,"查找数据成功",user)));
        }else{
            out.write(JSON.toJSONString(new Result(500,"查找数据失败",null)));
        }
    }

    private void add(HttpServletRequest request, PrintWriter out) {
        String userName = request.getParameter("userName");
        String passWord = request.getParameter("passWord");
        String nickName = request.getParameter("nickName");

        User user = new User(0, userName, passWord, nickName, new Date());
        userService = new UserServiceImpl();
        boolean bool =  userService.addUser(user);
        if (bool){
            out.write(JSON.toJSONString(new Result(200,"注册成功",null)));
        }else {
            out.write(JSON.toJSONString(new Result(500,"注册失败",null)));
        }
        out.close();
    }

    private void query(PrintWriter out, HttpServletRequest request, HttpServletResponse response) {
        List<User> users = userService.findAll();
        response.setContentType("text/html; charset=UTF-8");

        if (users != null && users.size() > 0){

            request.setAttribute("users",users);
            HttpSession session = request.getSession();
            if (session.getAttribute("user") == null){
                try {
                    response.sendRedirect(request.getContextPath()+"/login.jsp");
                    return;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            try {
                request.getRequestDispatcher("/table.jsp").forward(request,response);
            } catch (ServletException | IOException e) {
                e.printStackTrace();
            }
        }else{
            out.write("查找数据为空");
        }
    }
}
