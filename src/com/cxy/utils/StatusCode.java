package com.cxy.utils;

/**
 * 状态码
 * @author 26414
 */
public class StatusCode {
    public static final Integer OK = 200;
    public static final Integer ERROR = 400;
}
