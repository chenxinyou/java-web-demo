package com.cxy.service;

import com.cxy.pojo.User;

import java.util.List;

/**
 * @author 26414
 */
public interface UserService {
    /**
     * 查询全部
     * @return
     */
    List<User> findAll();

    /**
     * 添加user
     * @param user
     * @return
     */
    boolean addUser(User user);

    /**
     * 根据id查询User
     * @param id
     * @return
     */
    User findUserById(String id);

    /**
     * 修改User
     * @param user
     * @return
     */
    boolean editUser(User user);

    /**
     * 根据id删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 验证用户名是否占用
     * @param userName
     * @return
     */
    User ajaxYZ(String userName);

    /**
     * 批量删除
     * @param id
     * @return
     */
    boolean deleteAll(String[] id);

    /**
     * 关键字查询
     * @param keyword
     * @return
     */
    List<User> searchByKeyword(String keyword);

    /**
     * 登录
     * @param userName
     * @param passWord
     * @return
     */
    User login(String userName, String passWord);
}
