package com.cxy.service.impl;

import com.cxy.dao.UserDao;
import com.cxy.pojo.User;
import com.cxy.service.UserService;

import java.util.List;

/**
 * @author 26414
 */
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Override
    public List<User> findAll() {
        userDao = new UserDao();
        return userDao.findAll();
    }

    @Override
    public boolean addUser(User user) {
        userDao = new UserDao();
        int row =  userDao.addUser(user);
        return row > 0;
    }

    /**
     * 根据id查询User
     *
     * @param id
     * @return
     */
    @Override
    public User findUserById(String id) {
        userDao = new UserDao();
        return userDao.findUserById(id);
    }

    /**
     * 修改User
     *
     * @param user
     * @return
     */
    @Override
    public boolean editUser(User user) {
        userDao = new UserDao();
        int row = userDao.editUser(user);
        return  row>0;
    }

    /**
     * 根据id删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean delete(String id) {
        userDao = new UserDao();
        int row = userDao.delete(id);
        return row>0;
    }

    /**
     * 验证用户名是否占用
     *
     * @param userName
     * @return
     */
    @Override
    public User ajaxYZ(String userName) {
        userDao = new UserDao();

        return userDao.ajaxYZ(userName);
    }

    /**
     * 批量删除
     *
     * @param id
     * @return
     */
    @Override
    public boolean deleteAll(String[] id) {

        userDao = new UserDao();

        try {
            for (String s : id) {
               userDao.deleteAll(s);
            }
        } catch (Exception e) {
            System.out.println("批量删除失败");
            return false;
        }

        return true;
    }

    /**
     * 关键字查询
     *
     * @param keyword
     * @return
     */
    @Override
    public List<User> searchByKeyword(String keyword) {
        userDao = new UserDao();

        return userDao.searchByKeyword(keyword);
    }

    /**
     * 登录
     *
     * @param userName
     * @param passWord

     * @return
     */
    @Override
    public User login(String userName, String passWord) {
        userDao = new UserDao();

        return userDao.login(userName, passWord);
    }
}
